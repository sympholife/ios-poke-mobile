//
//  PokemonDetailsVC.swift
//  Pokedex Mobile
//
//  Created by Anthony Benitez on 6/22/17.
//  Copyright © 2017 SymphoLife. All rights reserved.
//

import UIKit

class PokemonDetailsVC: UIViewController {


    
    var pokemon: Pokemon!
    
    @IBOutlet weak var pkName: UILabel!
    @IBOutlet weak var pkImage: UIImageView!
    @IBOutlet weak var pkDescription: UILabel!
    @IBOutlet weak var pkHeight: UILabel!
    @IBOutlet weak var pkType: UILabel!
    @IBOutlet weak var pkWeight: UILabel!
    @IBOutlet weak var pkId: UILabel!
    @IBOutlet weak var pkAttack: UILabel!
    @IBOutlet weak var pkDefense: UILabel!
    @IBOutlet weak var pkNextEvolution: UILabel!
    @IBOutlet weak var pkCurrentEvolutionImage: UIImageView!
    @IBOutlet weak var pkNextEvolutionImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pkName.text = pokemon.name
        
        pokemon.downloadPokemonDetails {
            self.updateUI()
        }
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func updateUI() {
        pkHeight.text = pokemon.height
        pkWeight.text = pokemon.weight
        pkAttack.text = pokemon.attack
        pkDefense.text = pokemon.defense
        pkId.text = String(pokemon.pokedexId)
        pkDescription.text = pokemon.description
        pkType.text = pokemon.type
        pkImage.image = UIImage(named: "\(pokemon.pokedexId)")
        pkCurrentEvolutionImage.image = UIImage(named: "\(pokemon.pokedexId)")
        
        if pokemon.nextEvolutionId == "" {
            pkNextEvolution.text = "No Evolutions Discovered"
            pkNextEvolutionImage.isHidden = true
        } else {
            pkNextEvolutionImage.isHidden = false
            pkNextEvolutionImage.image = UIImage(named: "\(pokemon.nextEvolutionId)")
            
            let str = "Next Evolution: \(pokemon.nextEvolutionName) - Lv.\(pokemon.nextEvolutionLevel)"
            pkNextEvolution.text = str
        }
    }
}
