//
//  Constants.swift
//  Pods
//
//  Created by Anthony Benitez on 6/22/17.
//
//

import Foundation

let BASE_URL = "https://pokeapi.co/api/v1/pokemon/"

// Closure
typealias DownloadComplete = () -> ()
